from chatapp.forms import ConversationForm
from django.shortcuts import render
from django.views.generic import ListView, FormView,  View, TemplateView
from django.contrib.auth.models import User
from chatapp.models import Conversation, ConversationMessage
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

# Create your views here.
class UserList(ListView):
    model = User
    template_name = 'user_list.html'


class ConversationList(View):
    '''
    List the conversations and inactive users of the url's user
    '''
    user = None
    template_name = 'user_home.html'
    model = Conversation

    def get_inactives_users(self, conversations):
        '''Users without conversation'''
        user_actives = list()
        user_actives.append(self.user.id)
        for object in conversations:
            user_actives.append(object.get_other_user_id(self.user.id))
        inactive_users = User.objects.filter().exclude(id__in=user_actives)
        return  inactive_users


    def dispatch(self, request, *args, **kwargs):
        user_id = kwargs.get('user_id')       
        try:
            self.user = User.objects.get(id=user_id)
        except User.DoesNotExist:
            return HttpResponseRedirect(reverse('home'))
        return super(ConversationList, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        '''
        return the conversations and inactive users
        '''
        context = dict()
        context['user_id'] = self.user.id
        conversations = self.model.objects.filter(Q(user_1=self.user) | Q(user_2=self.user))
        context['conversations'] =conversations
        context['inactive_users'] = self.get_inactives_users(conversations)
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        return render(request, self.template_name, {
            'context': context
        })

    def post(self, request, *args, **kwargs):
        other_user = request.POST.get('other_user', None)
        if other_user:
            try:
               other_user = User.objects.get(id=other_user)
            except User.DoesNotExist:
                return HttpResponseRedirect(reverse('home'))
        else:
            context = self.get_context_data()
            return render(request, self.template_name, {'context': context})
        
        if other_user and (not Conversation.objects.filter(user_1=self.user, user_2=other_user).exists()
                           and not Conversation.objects.filter(user_1=other_user, user_2=self.user).exists()):
            # if the conversation doesn't exists and if we get the other_user parameter, the conversation is created
            conversation = Conversation(user_1=self.user, user_2=other_user)
            conversation.save()
            return HttpResponseRedirect(reverse('conversation', kwargs={'user_id': self.user.id, 'conversation_id': conversation.id}))




class ConversationView(FormView):
    '''
    list the messages from the conversation
    add neww messages
    '''
    main_user = None
    oher_user = None
    conversation = None
    form_class = ConversationForm
    template_name = 'conversation.html'

    def create_message(self, message):
        self.conversation.number_messages = self.conversation.number_messages + 1
        self.conversation.save()
        message_object = ConversationMessage(user=self.main_user,message=message,conversation=self.conversation)
        message_object.save()


    def get_messages(self):
        try:
            messages = ConversationMessage.objects.filter(conversation=self.conversation).order_by('date')
        except ConversationMessage.DoesNotExist:
            return HttpResponseRedirect(reverse('home'))
        return messages

    def dispatch(self, request, *args, **kwargs):
        main_user_id = kwargs.get('user_id')
        conversation_id = kwargs.get('conversation_id')
        try:
            self.main_user = User.objects.get(id=main_user_id)
        except User.DoesNotExist:
            return HttpResponseRedirect(reverse('home'))
        try:
            self.conversation = Conversation.objects.get(id=conversation_id)
        except Conversation.DoesNotExist:
            return HttpResponseRedirect(reverse('home'))
        return super(ConversationView, self).dispatch(request, *args, **kwargs)


    def get_context_data(self, **kwargs):
        context = super(ConversationView, self).get_context_data(**kwargs)
        context['messages'] = self.get_messages()
        context['main_user'] = self.main_user
        context['other_user'] = User.objects.get(id=(self.conversation.get_other_user_id(self.main_user.id))).username
        context['form'] = self.form_class(initial={'user':self.main_user.id})
        return context

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST)
        if form.is_valid():
            self.create_message(form.cleaned_data['message'])
        return HttpResponseRedirect(reverse('conversation', kwargs={'user_id': self.main_user.id, 'conversation_id': self.conversation.id}))















