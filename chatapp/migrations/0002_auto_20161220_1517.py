# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-12-20 15:17
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('chatapp', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='OperationMessage',
            new_name='ConversationMessage',
        ),
        migrations.AlterField(
            model_name='conversation',
            name='user_1',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='User1', to=settings.AUTH_USER_MODEL, verbose_name='Usuario 1'),
        ),
        migrations.AlterField(
            model_name='conversation',
            name='user_2',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='User2', to=settings.AUTH_USER_MODEL, verbose_name='Usuario 2'),
        ),
    ]
