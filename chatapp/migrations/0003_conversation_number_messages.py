# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-12-20 17:04
from __future__ import unicode_literals

from django.db import migrations, models


def get_number_of_messages(apps, schema_editor):
    ConversationMessages = apps.get_model('chatapp', 'ConversationMessage')
    Conversation = apps.get_model('chatapp', 'Conversation')
    conversations = Conversation.objects.all()
    for object in conversations:
        object.number_messages = len(ConversationMessages.objects.filter(conversation=object))
        object.save()




class Migration(migrations.Migration):

    dependencies = [
        ('chatapp', '0002_auto_20161220_1517'),
    ]

    operations = [
        migrations.AddField(
            model_name='conversation',
            name='number_messages',
            field=models.IntegerField(default=0, verbose_name='Cantidad de Mensajes'),
        ),

    migrations.RunPython(get_number_of_messages),
    ]
