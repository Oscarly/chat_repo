from django.contrib import admin
from chatapp import models

# Register your models here.

class ConversationAdmin(admin.ModelAdmin):
    list_display = ['user_1', 'user_2','number_messages']


class MessageAdmin(admin.ModelAdmin):
    list_display = ['user', 'date', 'message']

admin.site.register(models.Conversation, ConversationAdmin)
admin.site.register(models.ConversationMessage, MessageAdmin)