from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

# Create your models here.


class Conversation(models.Model):
    """ """
    user_1 = models.ForeignKey(User, related_name=_('User1'), blank=True, verbose_name=_('Usuario 1'))
    user_2 = models.ForeignKey(User, related_name=_('User2'),  blank=True, verbose_name=_('Usuario 2'))
    number_messages = models.IntegerField(default=0, verbose_name=_('Cantidad de Mensajes'))


    def get_other_user_id(self, id):
        user_id = self.user_1.id
        if self.user_1.id == id:
            user_id = self.user_2.id
        return user_id


class ConversationMessage(models.Model):
    """ Mensajes de cada conversación """
    user = models.ForeignKey(User,  verbose_name=_('Usuario'))
    conversation = models.ForeignKey(Conversation)
    message = models.TextField()
    date = models.DateTimeField(default=timezone.now)