from chatapp.models import Conversation
from django import template

register = template.Library()


@register.filter(name='get_conversation_name')
def get_conversation_name(conversation_id, user_id):
    conversation  = Conversation.objects.get(id=conversation_id)
    name = conversation.user_1.username
    if conversation.user_1.id == user_id:
        name = conversation.user_2.username
    return name

